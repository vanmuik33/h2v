<?php
add_action( 'wp_enqueue_scripts', 'h2v_style_script',999 );
function h2v_style_script() {
    wp_enqueue_style(
        'h2v-main',
        get_template_directory_uri() . '/assets/css/main.css',
        [],
        HELLO_ELEMENTOR_VERSION
    );
}